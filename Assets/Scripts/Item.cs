using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Item : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject item;

    public void OnPointerDown(PointerEventData eventData)
    {
        audioSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        item.transform.localScale *= 1.5f;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        item.transform.localScale = new Vector3(1f, 1f);
    }
}
